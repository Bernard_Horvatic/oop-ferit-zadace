#ifndef Episode_H
#define Episode_H

class Episode {
private:
	int viewerCount;
	float scoreSum;
	float maxScore;
public:
	Episode();
	Episode(int, float, float);
	int getViewerCount();
	void addView(float);
	float getMaxScore();
	float getAverageScore();
	void setEpisode(int, float, float);
};
float generateRandomScore();

#endif