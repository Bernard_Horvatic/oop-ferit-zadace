#include <iostream>
#include <stdlib.h>
#include "Episode.h"
using namespace std;



Episode::Episode() {
	viewerCount = 0;
	scoreSum = 0;
	maxScore = 0;
}
Episode::Episode(int v, float s, float m) {
	viewerCount = v;
	scoreSum = s;
	maxScore = m;

}
int Episode::getViewerCount() {
	return viewerCount;
}
float Episode::getAverageScore() {
	return (scoreSum / viewerCount);
}
float Episode::getMaxScore() {
	return maxScore;
}
void Episode::setEpisode(int v, float s, float m) {
	viewerCount = v;
	scoreSum = s;
	maxScore = m;
}
void Episode::addView(float s) {
	viewerCount++;
	scoreSum += s;
	if (s > maxScore) {
		maxScore = s;
	}
}

float generateRandomScore() {
	//return ((float)(rand() % 101)) / 10.0;
	return (float)rand() / RAND_MAX * 10;
}